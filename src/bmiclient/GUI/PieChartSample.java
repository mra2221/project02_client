package bmiclient.GUI;

import bmiclient.Main;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.chart.PieChart;
import javafx.stage.Stage;

public class PieChartSample extends Application {
    @Override
    public void start(Stage stage) {
        Scene scene = new Scene(new Group());
        stage.setTitle("BMI Statistic");
        stage.setWidth(500);
        stage.setHeight(500);
        double allRecords = Integer.parseInt(Main.statisticData.getNormal())+Integer.parseInt(Main.statisticData.getEmaciation())+Integer.parseInt(Main.statisticData.getUnderweight())+Integer.parseInt(Main.statisticData.getOverweigt())+Integer.parseInt(Main.statisticData.getObesityI())+Integer.parseInt(Main.statisticData.getObesityII())+Integer.parseInt(Main.statisticData.getObesityIII())+Integer.parseInt(Main.statisticData.getStarvation());
        String normal = Main.statisticData.getNormal();
        String emaciation = Main.statisticData.getEmaciation();
        String underweight = Main.statisticData.getUnderweight();
        String overweight = Main.statisticData.getOverweigt();
        String obesityI = Main.statisticData.getObesityI();
        String obesityII = Main.statisticData.getObesityII();
        String obesityIII = Main.statisticData.getObesityIII();
        String starvation = Main.statisticData.getStarvation();
        ObservableList<PieChart.Data> pieChartData = FXCollections.observableArrayList();

        if(Integer.parseInt(starvation) > 0) pieChartData.add(new PieChart.Data("Wygłodzenie " + Math.round((Integer.parseInt(starvation)/allRecords)*100) + "%", Integer.parseInt(starvation)));
        if(Integer.parseInt(emaciation) > 0) pieChartData.add(new PieChart.Data("Wychudzenie "+ Math.round((Integer.parseInt(emaciation)/allRecords)*100) + "%", Integer.parseInt(emaciation)));
        if(Integer.parseInt(underweight) > 0) pieChartData.add(new PieChart.Data("Niedowaga "+ Math.round((Integer.parseInt(underweight)/allRecords)*100) + "%", Integer.parseInt(underweight)));
        if(Integer.parseInt(normal) > 0) pieChartData.add(new PieChart.Data("W normie "+ Math.round((Integer.parseInt(normal)/allRecords)*100) + "%", Integer.parseInt(normal)));
        if(Integer.parseInt(overweight) > 0) pieChartData.add( new PieChart.Data("Nadwaga "+ Math.round((Integer.parseInt(overweight)/allRecords)*100) + "%", Integer.parseInt(overweight)));
        if(Integer.parseInt(obesityI) > 0) pieChartData.add(new PieChart.Data("Otyłość I "+ Math.round((Integer.parseInt(obesityI)/allRecords)*100) + "%", Integer.parseInt(obesityI)));
        if(Integer.parseInt(obesityII) > 0) pieChartData.add(new PieChart.Data("Otyłość II "+ Math.round((Integer.parseInt(obesityII)/allRecords)*100) + "%", Integer.parseInt(obesityII)));
        if(Integer.parseInt(obesityIII) > 0) pieChartData.add(new PieChart.Data("Otyłość III "+ Math.round((Integer.parseInt(obesityIII)/allRecords)*100) + "%", Integer.parseInt(obesityIII)));
        final PieChart chart = new PieChart(pieChartData);
        chart.setTitle("BMI Statistic");
        ((Group) scene.getRoot()).getChildren().add(chart);
        stage.setScene(scene);
        stage.show();
    }

}


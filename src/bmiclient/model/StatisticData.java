package bmiclient.model;

public class StatisticData {
    public String starvation;
    public String emaciation;
    public String underweight;
    public String normal;
    public String overweigt;
    public String obesityI;
    public String obesityII;
    public String obesityIII;

    public StatisticData(){
        this.starvation="0";
        this.emaciation="0";
        this.underweight="0";
        this.normal="0";
        this.overweigt="0";
        this.obesityI="0";
        this.obesityII="0";
        this.obesityIII="0";
    }

    public StatisticData(String starvation,
                         String emaciation,
                         String underweight,
                         String normal,
                         String overweight,
                         String obesityI,
                         String obesityII,
                         String obesityIII){
        this.starvation=starvation;
        this.emaciation=emaciation;
        this.underweight=underweight;
        this.normal=normal;
        this.overweigt=overweight;
        this.obesityI=obesityI;
        this.obesityII=obesityII;
        this.obesityIII=obesityIII;
    }
    public String getStarvation(){
        return starvation;
    }

    public String getEmaciation() {
        return emaciation;
    }

    public String getNormal() {
        return normal;
    }

    public String getUnderweight() {
        return underweight;
    }

    public String getOverweigt() {
        return overweigt;
    }

    public String getObesityI() {
        return obesityI;
    }

    public String getObesityII() {
        return obesityII;
    }

    public String getObesityIII() {
        return obesityIII;
    }
    public void setStarvation(String starvation){
        this.starvation = String.valueOf(starvation);
    }
    public void setEmaciation(String emaciation){
        this.emaciation = String.valueOf(emaciation);
    }
    public void setUnderweight(String underweight){
        this.underweight = String.valueOf(underweight);
    }
    public  void setNormal(String normal){
        this.normal = String.valueOf(normal);
    }
    public void setOverweigt(String overweigt){
        this.overweigt = String.valueOf(overweigt);
    }
    public void setObesityI(String obesityI){
        this.obesityI = String.valueOf(obesityI);
    }
    public void setObesityII(String obesityII){
        this.obesityII = String.valueOf(obesityII);
    }
    public void setObesityIII(String obesityIII){
        this.obesityIII = String.valueOf(obesityIII);
    }

}
